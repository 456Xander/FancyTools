package at.xander.fancytools.config;

import net.minecraft.world.item.Tier;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.common.ForgeConfigSpec;

/**
 * This is an implementation if ItemTier, where most parameters will be defined
 * by a config file.
 * 
 * @implNote All Floating-Point Parameters are saved as Double in the config,
 *           because the Config Parser does not correctly recognize Float config
 *           and will parse it as Double, which then results in a
 *           ClassCastException
 * 
 * @author Alexander Daum
 *
 */
public class ConfigurableMaterial implements Tier {
	private final FancyMaterial material;

	private final DefaultableConfigValue<Integer> durability;
	private final DefaultableConfigValue<Double> speed;
	private final DefaultableConfigValue<Double> attackDamage;
	private final DefaultableConfigValue<Integer> harvestLevel;
	private final DefaultableConfigValue<Integer> enchantmentValue;
	private final DefaultableConfigValue<Double> axeSpeed;

	public ConfigurableMaterial(FancyMaterial material, ForgeConfigSpec.Builder builder) {
		this.material = material;

		builder.push(material.name());
		durability = DefaultableConfigValue.wrap(builder.define("durability", material.getUses()));
		speed = DefaultableConfigValue.wrap(
				builder.comment("Defines how fast the tools will break blocks. For Reference:Iron = 6, Diamond = 8")
						.define("harvest-speed", (double) material.getSpeed()));
		attackDamage = DefaultableConfigValue.wrap(builder
				.comment("Defines how much damage weapons deal,"
						+ " this is a modifier to the base damage. Iron = 2, Diamond = 3")
				.define("attack-damage", (double) material.getAttackDamageBonus()));
		harvestLevel = DefaultableConfigValue.wrap(builder.comment("0 = Wood, 1 = Stone, 2 = Iron, 3 = Diamond")
				.define("harvest-level", material.getHarvestLevel()));
		enchantmentValue = DefaultableConfigValue
				.wrap(builder.comment("A higher value means better enchantments. Iron = 14, Diamond = 10, Gold = 22")
						.define("enchantment-value", material.getEnchantment()));
		axeSpeed = DefaultableConfigValue
				.wrap(builder.comment("The attack speed of axes, should be negative. Diamond = -3.0, Iron = -3.1")
						.define("axe-speed", (double) material.getAxeSpeed()));
		builder.pop();
	}

	@Override
	public int getUses() {
		return durability.get();
	}

	@Override
	public float getSpeed() {
		return speed.get().floatValue();
	}

	@Override
	public float getAttackDamageBonus() {
		return attackDamage.get().floatValue();
	}

	@Override
	public int getLevel() {
		return harvestLevel.get();
	}

	@Override
	public int getEnchantmentValue() {
		return enchantmentValue.get();
	}

	@Override
	public Ingredient getRepairIngredient() {
		return material.getRepairIngredient();
	}

	public float getAxeSpeed() {
		return axeSpeed.get().floatValue();
	}

}
