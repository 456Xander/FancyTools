package at.xander.fancytools.config;

import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.fml.event.config.ModConfigEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

/**
 * Wrapper for a {@link ConfigValue}, that returns the default value if
 * requested before the config is lodaded.
 * 
 * @author Alexander Daum
 *
 * @param <T>
 */
public class DefaultableConfigValue<T> {
	private final ConfigValue<T> cfg;
	private boolean cfgLoaded = false;

	public DefaultableConfigValue(ConfigValue<T> cv) {
		cfg = cv;
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onConfigLoaded);
	}

	public static <T> DefaultableConfigValue<T> wrap(ConfigValue<T> cv) {
		return new DefaultableConfigValue<>(cv);
	}

	public T get() {
		return cfgLoaded ? cfg.get() : cfg.getDefault();
	}

	private void onConfigLoaded(ModConfigEvent e) {
		cfgLoaded = true;
	}
}
