package at.xander.fancytools.config;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;

public enum FancyMaterial {
	Obsidian(2500, 7.0f, 2.0f, 3, 5, -3.1f, Ingredient.of(new ItemStack(Items.OBSIDIAN))),
	Emerald(1000, 8.0f, 2.5f, 3, 15, -3.0f, Ingredient.of(new ItemStack(Items.EMERALD)));

	private final int uses;
	private final float speed;
	private final float attackDamageBonus;
	private final int harvestLevel;
	private final int enchantment;
	private final float axeSpeed;
	private final Ingredient repairIngredient;

	private FancyMaterial(int uses, float speed, float attackDamageBonus, int harvestLevel, int enchantment,
			float axeSpeed, Ingredient repairIngredient) {
		this.uses = uses;
		this.speed = speed;
		this.attackDamageBonus = attackDamageBonus;
		this.harvestLevel = harvestLevel;
		this.enchantment = enchantment;
		this.axeSpeed = axeSpeed;
		this.repairIngredient = repairIngredient;
	}

	public int getUses() {
		return uses;
	}

	public float getSpeed() {
		return speed;
	}

	public float getAttackDamageBonus() {
		return attackDamageBonus;
	}

	public int getHarvestLevel() {
		return harvestLevel;
	}

	public int getEnchantment() {
		return enchantment;
	}

	public float getAxeSpeed() {
		return axeSpeed;
	}

	public Ingredient getRepairIngredient() {
		return repairIngredient;
	}
}
