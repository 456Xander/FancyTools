package at.xander.fancytools.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig.Type;

public class FancyToolsConfig {
	private ForgeConfigSpec config;

	private final ConfigurableMaterial obsidian;
	private final ConfigurableMaterial emerald;

	public FancyToolsConfig() {
		ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
		obsidian = new ConfigurableMaterial(FancyMaterial.Obsidian, builder);
		emerald = new ConfigurableMaterial(FancyMaterial.Emerald, builder);
		config = builder.build();
	}

	public void registerConfig(ModLoadingContext ctx) {
		ctx.registerConfig(Type.COMMON, config);
	}

	public ConfigurableMaterial getObsidian() {
		return obsidian;
	}

	public ConfigurableMaterial getEmerald() {
		return emerald;
	}

}
