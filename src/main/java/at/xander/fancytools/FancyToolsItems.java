package at.xander.fancytools;

import com.google.common.collect.ImmutableList;

import at.xander.fancytools.config.ConfigurableMaterial;
import at.xander.fancytools.config.FancyMaterial;
import at.xander.fancytools.config.FancyToolsConfig;
import at.xander.fancytools.items.DynamicAxe;
import at.xander.fancytools.items.DynamicHoe;
import at.xander.fancytools.items.DynamicPickaxe;
import at.xander.fancytools.items.DynamicShovel;
import at.xander.fancytools.items.DynamicSword;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class FancyToolsItems {
	private final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, FancyToolsMod.MODID);
	private final CreativeTabHelper ctHelper;

	private ConfigurableMaterial getConfig(FancyMaterial material) {
		FancyToolsConfig cfg = FancyToolsMod.getInstance().getConfig();
		switch (material) {
		case Obsidian:
			return cfg.getObsidian();
		case Emerald:
			return cfg.getEmerald();
		}
		throw new IllegalArgumentException("The FancyMaterial is unknown, material is: " + material.name());
	}

	private Item createAxe(FancyMaterial material) {
		return new DynamicAxe(getConfig(material), 8.0f, material.getAxeSpeed(), new Item.Properties());
	}

	private Item createPickaxe(FancyMaterial material) {
		return new DynamicPickaxe(getConfig(material), 1, -2.8f, new Item.Properties());
	}

	private Item createSword(FancyMaterial material) {
		return new DynamicSword(getConfig(material), 3, -2.4f, new Item.Properties());
	}

	private Item createHoe(FancyMaterial material) {
		return new DynamicHoe(getConfig(material), -1.0f, new Item.Properties());
	}

	private Item createShovel(FancyMaterial material) {
		return new DynamicShovel(getConfig(material), 1.5f, -3.0f, new Item.Properties());
	}

	public FancyToolsItems(FMLJavaModLoadingContext ctx) {
		ctHelper = new CreativeTabHelper(ctx.getModEventBus());
		ITEMS.register(ctx.getModEventBus());

		for (var material : ImmutableList.of(FancyMaterial.Emerald, FancyMaterial.Obsidian)) {
			var axe = ITEMS.register(material.name().toLowerCase() + "_axe", () -> createAxe(material));
			var hoe = ITEMS.register(material.name().toLowerCase() + "_hoe", () -> createHoe(material));
			var pickaxe = ITEMS.register(material.name().toLowerCase() + "_pickaxe", () -> createPickaxe(material));
			var shovel = ITEMS.register(material.name().toLowerCase() + "_shovel", () -> createShovel(material));
			var sword = ITEMS.register(material.name().toLowerCase() + "_sword", () -> createSword(material));
			ctHelper.addItemToCreativeTab(axe, CreativeModeTabs.TOOLS_AND_UTILITIES);
			ctHelper.addItemToCreativeTab(hoe, CreativeModeTabs.TOOLS_AND_UTILITIES);
			ctHelper.addItemToCreativeTab(pickaxe, CreativeModeTabs.TOOLS_AND_UTILITIES);
			ctHelper.addItemToCreativeTab(shovel, CreativeModeTabs.TOOLS_AND_UTILITIES);
			ctHelper.addItemToCreativeTab(sword, CreativeModeTabs.COMBAT);
		}
	}
}
