package at.xander.fancytools;

import at.xander.fancytools.config.FancyToolsConfig;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(FancyToolsMod.MODID)
public class FancyToolsMod {
	public static final String MODID = "fancytools";

	private final FancyToolsConfig config;

	private static FancyToolsMod instance;
	
	public final FancyToolsItems items;

	public FancyToolsMod() {
		instance = this;
		config = new FancyToolsConfig();
		config.registerConfig(ModLoadingContext.get());
		items = new FancyToolsItems(FMLJavaModLoadingContext.get());
	}

	public static FancyToolsMod getInstance() {
		return instance;
	}

	public FancyToolsConfig getConfig() {
		return config;
	}

}
