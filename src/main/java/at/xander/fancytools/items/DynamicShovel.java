package at.xander.fancytools.items;

import com.google.common.collect.Multimap;

import net.minecraft.util.Mth;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.block.state.BlockState;

/**
 * This pickaxe can handle dynamically changing Tool Stats
 * 
 * @author Alexander Daum
 *
 */
public class DynamicShovel extends ShovelItem {
	private final float base_atk_damage;
	private final float attackSpeed;

	public DynamicShovel(Tier material, float attack_damage, float attack_speed, Properties properties) {
		super(material, attack_damage, attack_speed, properties);
		this.base_atk_damage = attack_damage;
		this.attackSpeed = attack_speed;
	}

	public float getAttackDamage() {
		return base_atk_damage + getTier().getAttackDamageBonus();
	}

	private double getAttackSpeed() {
		return (double) attackSpeed;
	}

	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state) {
		float destroySpeed = super.getDestroySpeed(stack, state);
		// When the destroySpeed == super.speed, then use the dynamic speed
		return destroySpeed == super.speed ? getTier().getSpeed() : destroySpeed;
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot) {
		return DynamicToolHelper.getDefaultAttributeModifiers(slot, getAttackDamage(), getAttackSpeed());
	}

	@Override
	public int getMaxDamage(ItemStack stack) {
		return getTier().getUses();
	}
	@Override
	public int getBarWidth(ItemStack stack) {
		return Math.round(13.0F - (float) stack.getDamageValue() * 13.0F / (float) stack.getMaxDamage());
	}

	@Override
	public int getBarColor(ItemStack stack) {
		float f = Math.max(0.0F,
				((float) (stack.getMaxDamage() - stack.getDamageValue())) / (float) stack.getMaxDamage());
		return Mth.hsvToRgb(f / 3.0F, 1.0F, 1.0F);
	}
}
