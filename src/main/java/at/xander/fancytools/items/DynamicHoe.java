package at.xander.fancytools.items;

import com.google.common.collect.Multimap;

import net.minecraft.util.Mth;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;

/**
 * This pickaxe can handle dynamically changing Tool Stats
 * 
 * @author Alexander Daum
 *
 */
public class DynamicHoe extends HoeItem {
	private final float attackSpeed;

	public DynamicHoe(Tier material, float attack_speed, Properties properties) {
		super(material, 0, attack_speed, properties);
		this.attackSpeed = attack_speed;
	}

	public float getAttackDamage() {
		return 0.0f;
	}

	private double getAttackSpeed() {
		return (double) attackSpeed;
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot) {
		return DynamicToolHelper.getDefaultAttributeModifiers(slot, getAttackDamage(), getAttackSpeed());
	}

	@Override
	public int getMaxDamage(ItemStack stack) {
		return getTier().getUses();
	}
	@Override
	public int getBarWidth(ItemStack stack) {
		return Math.round(13.0F - (float) stack.getDamageValue() * 13.0F / (float) stack.getMaxDamage());
	}

	@Override
	public int getBarColor(ItemStack stack) {
		float f = Math.max(0.0F,
				((float) (stack.getMaxDamage() - stack.getDamageValue())) / (float) stack.getMaxDamage());
		return Mth.hsvToRgb(f / 3.0F, 1.0F, 1.0F);
	}
}
