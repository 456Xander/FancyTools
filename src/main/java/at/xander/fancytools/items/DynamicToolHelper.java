package at.xander.fancytools.items;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.Item;

/**
 * Helper Class for Dynamic Tool Items.
 * 
 * This class extends Item because it needs access to a protected static Field
 * of the Item class. This class does NOT represent a Minecraft Item and cannot
 * be instantiated
 * 
 * @author Alexander Daum
 *
 */
class DynamicToolHelper extends Item {
	static Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot, double damage,
			double speed) {
		Multimap<Attribute, AttributeModifier> multimap = HashMultimap.create();
		if (slot == EquipmentSlot.MAINHAND) {
			multimap.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Tool modifier",
					damage, AttributeModifier.Operation.ADDITION));
			multimap.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Tool modifier", speed,
					AttributeModifier.Operation.ADDITION));
		}
		return multimap;
	}

	/**
	 * This class should never be instantiated
	 */
	private DynamicToolHelper() {
		super(null);
		throw new RuntimeException("This class should not be instantiated");
	}
}
