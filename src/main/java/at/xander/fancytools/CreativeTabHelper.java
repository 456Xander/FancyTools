package at.xander.fancytools;

import java.util.function.Supplier;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.eventbus.api.IEventBus;

/**
 * Class to help creative tab registration using the new event mechanism.
 * 
 * @author Alexander Daum
 */
public class CreativeTabHelper {
	
	private Multimap<ResourceKey<CreativeModeTab>, Supplier<?extends ItemLike>> items;
	
	/**
	 * Create a new {@link CreativeTabHelper} and attach it to the eventBus
	 * 
	 * @param eventBus the event Bus
	 */
	public CreativeTabHelper(IEventBus eventBus) {
		items = MultimapBuilder.hashKeys().arrayListValues().build();
		eventBus.addListener(this::onBuildCreativeTabContent);
	}
	
	/**
	 * Add an item creative tab mapping
	 * @param item A Supplier for an Item, can be a RegistryObject<Item>
	 * @param tab The tab in which the item should be added
	 */
	public void addItemToCreativeTab(Supplier<?extends ItemLike> item, ResourceKey<CreativeModeTab> tab) {
		items.put(tab, item);		
	}

	private void onBuildCreativeTabContent(BuildCreativeModeTabContentsEvent event) {
		items.get(event.getTabKey()).forEach(event::accept);
	}
}
